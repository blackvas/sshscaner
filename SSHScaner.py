# -*- coding: utf-8 -*-

import paramiko as ssh
import datetime as d_t
from tkinter import *
from tkinter.ttk import Combobox

def now ():
    now = d_t.datetime.now ()
    return str (now.day) + "-" + str (now.month) + "-" + str (now.year)

def scan ():
    image_name = "{}".format (name_image.get ())
    file_ras = "{}".format (ras_file.get ())
    color_var = "{}".format (var_color.get ())
    file_dpi = "{}".format (dpi.get ())
    
    print (image_name, file_ras, color_var, file_dpi)
    
    if color_var == "Цветной":
        color_var = "Color"
    elif color_var == "Чёрно-белый":
        color_var = "Black"
    
    host_local = "{}".format (host.get ())
    user_local = "{}".format (user.get ())
    passwd_local = "{}".format (passwd.get ())
    port_local = "{}".format (port.get ())
    
    print (host_local, user_local, passwd_local, port_local)
    
    command = "scanimage -d \"hpaio:/usb/Deskjet_2510_series?serial=CN32R3JQTP05TX\" --resolution " + file_dpi + " --mode " + color_var + " --format=" + file_ras + " > " + image_name + "." + file_ras 
    
    print (command)
    
    client = ssh.SSHClient ()
    client.set_missing_host_key_policy (ssh.AutoAddPolicy ())
    client.connect (hostname = host_local, username = user_local, password = passwd_local, port = port_local)
    stdin, stdout, stderr = client.exec_command ("cd /media/pi/IP_FILES/'Скан'")
    data = stdout.read () + stderr.read ()
    print (data)
    #stdin, stdout, stderr = client.exec_command ("mkdir " + now ())
    #data = stdout.read () + stderr.read ()
    #print (data)
    stdin, stdout, stderr = client.exec_command (command)
    data = stdout.read () + stderr.read ()
    print (data)
    client.close ()
    
window = Tk ()
window.title ("Сканирование .... ")
window.geometry ("600x600")

h_name_image = Label (window, text = "Название файла:")
h_name_image.grid (column = 0, row = 1)
name_image = Entry (window, width = 50)
name_image.grid (column = 0, row = 2)

h_ras_file = Label (window, text = "Расширение файла:")
h_ras_file.grid (column = 0, row = 3)
ras_file = Combobox (window)
ras_file ["values"] = ("jpg", "png", "pdf")
ras_file.current (1)
ras_file.grid (column = 0, row = 4)

h_var_color = Label (window, text = "Режим сканирования:")
h_var_color.grid (column = 0, row = 5)
var_color = Combobox (window)
var_color ["values"] = ("Цветной", "Чёрно-белый")
var_color.current (0)
var_color.grid (column = 0, row = 6)

h_dpi = Label (window, text = "Разрешение:")
h_dpi.grid (column = 0, row = 7)
dpi = Combobox (window)
dpi ["values"] = (100, 200, 300, 400, 500, 600)
dpi.current (2)
dpi.grid (column = 0, row = 8)

h_host = Label (window, text = "IP-адрес:")
h_host.grid (column = 0, row = 9)
host = Entry (window, width = 50)
host.grid (column = 0, row = 10)

h_user = Label (window, text = "Логин:")
h_user.grid (column = 0, row = 11)
user = Entry (window, width = 50)
user.grid (column = 0, row = 12)

h_passwd = Label (window, text = "Пароль:")
h_passwd.grid (column = 0, row = 13)
passwd = Entry (window, show="*", width = 50)
passwd.grid (column = 0, row = 14)

h_port = Label (window, text = "Порт:")
h_port.grid (column = 0, row = 15)
port = Entry (window, width = 50)
port.grid (column = 0, row = 16)

btn = Button (window, text = "Сканировать", font = ("Arial", 12), command = scan)
btn.grid (column = 0, row = 17)

window.mainloop ()
